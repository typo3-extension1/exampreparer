<?php

use Hagenbreak\ExamPractice\Controller\ExamController;

defined('TYPO3') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'ExamPractice',
        'Takeexam',
        [
            ExamController::class => 'takeexam, create, result',
        ],
        // non-cacheable actions
        [
            ExamController::class => 'takeexam, create, result',
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    takeexam {
                        iconIdentifier = exam_practice-plugin-takeexam
                        title = LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exam_practice_takeexam.name
                        description = LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exam_practice_takeexam.description
                        tt_content_defValues {
                            CType = list
                            list_type = exampractice_takeexam
                        }
                    }
                }
                show = *
            }
       }'
    );
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['examPractice'][] = Hagenbreak\ExamPractice\ViewHelpers::class;
})();
