<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_exampractice_domain_model_poolquestion', 'EXT:exam_practice/Resources/Private/Language/locallang_csh_tx_exampractice_domain_model_poolquestion.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_exampractice_domain_model_poolquestion');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_exampractice_domain_model_poolanswer', 'EXT:exam_practice/Resources/Private/Language/locallang_csh_tx_exampractice_domain_model_poolanswer.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_exampractice_domain_model_poolanswer');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_exampractice_domain_model_exam', 'EXT:exam_practice/Resources/Private/Language/locallang_csh_tx_exampractice_domain_model_exam.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_exampractice_domain_model_exam');
})();
