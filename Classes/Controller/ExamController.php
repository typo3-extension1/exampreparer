<?php

namespace Hagenbreak\ExamPractice\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Hagenbreak\ExamPractice\Domain\Repository\PoolQuestionRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Hagenbreak\ExamPractice\Domain\Model\Exam;
use Hagenbreak\ExamPractice\Domain\Repository\ExamRepository;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class ExamController extends ActionController
{
    /**
     * @var PoolQuestionRepository
     */
    private $poolQuestionRepository;

    /**
     * @var ExamRepository
     */
    private $examRepository;

    protected const QUESTIONS_PER_EXAM = 5;

    public function __construct(
        PoolQuestionRepository $poolQuestionRepository,
        ExamRepository $examRepository
    ) {
        $this->poolQuestionRepository = $poolQuestionRepository;
        $this->examRepository = $examRepository;
    }

    public function takeexamAction()
    {
        $newExam = GeneralUtility::makeInstance(Exam::class);

        // get questions, set them as ObjectStorage and add them to the Exam model
        $questions = $this->poolQuestionRepository->findRandomQuestions(self::QUESTIONS_PER_EXAM);

        $objectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        foreach($questions as $question) {
            $question = $this->poolQuestionRepository->findByUid($question['uid']);
            $objectStorage->attach($question);
        }
        $newExam->setQuestions($objectStorage);
        $this->examRepository->add($newExam);
        GeneralUtility::makeInstance(PersistenceManager::class)->persistAll();
        $this->view->assign('exam', $newExam);
    }

    /**
     * @param Exam $exam
     */
    public function createAction(Exam $exam)
    {
        $this->examRepository->update($exam);
        GeneralUtility::makeInstance(PersistenceManager::class)->persistAll();
        $this->forward('result', null, null, ['exam' => $exam]);
    }

    public function resultAction(Exam $exam)
    {
        $correctAnswers = [];
        $givenAnswers = [];
        foreach($exam->getQuestions() as $question) {
            foreach($question->getAnswers() as $answer) {
                if($answer->getIsCorrect()) {
                    $correctAnswers[] = $answer->getUid();
                }
            }
        }
        foreach($exam->getAnswers() as $answer) {
            $givenAnswers[] = $answer->getUid();
        }
        $this->view->assignMultiple([
            'exam' => $exam,
            'correctAnswers' => $correctAnswers,
            'givenAnswers' => $givenAnswers
        ]);
    }
}
