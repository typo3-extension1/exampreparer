<?php

namespace Hagenbreak\ExamPractice\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ContainsViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument('array', 'array', 'The array', true);
        $this->registerArgument('item', 'mixed', 'The item to look for', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $array = $arguments['array'];
        $item = $arguments['item'];

        return in_array($item, $array, true);
    }
}
