<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/**
 * This file is part of the "Exam Practice" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023
 */

/**
 * The repository for PoolQuestions
 */
class PoolQuestionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function findRandomQuestions(int $limit = 10)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_exampractice_domain_model_poolquestion');

        $questions = $queryBuilder
            ->select('*')
            ->from('tx_exampractice_domain_model_poolquestion')
            ->addSelectLiteral('RAND() as random')
            ->orderBy('random')
            ->setMaxResults($limit)
            ->execute()
            ->fetchAllAssociative();
        return $questions;
    }
}
