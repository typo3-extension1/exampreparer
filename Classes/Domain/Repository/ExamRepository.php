<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Domain\Repository;

/**
 * This file is part of the "Exam Practice" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023
 */

/**
 * The repository for Exams
 */
class ExamRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
