<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Domain\Model;

/**
 * This file is part of the "Exam Practice" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023
 */

/**
 * PoolAnswer
 */
class PoolAnswer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * answer
     *
     * @var string
     */
    protected $answer = null;

    /**
     * isCorrect
     *
     * @var bool
     */
    protected $isCorrect = null;


    /**
     * question
     *
     * @var \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion
     */
    protected $poolquestion = null;

    /**
     * Returns the answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Sets the answer
     *
     * @param string $answer
     * @return void
     */
    public function setAnswer(string $answer)
    {
        $this->answer = $answer;
    }

    /**
     * Returns the isCorrect
     *
     * @return bool
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Sets the isCorrect
     *
     * @param bool $isCorrect
     * @return void
     */
    public function setIsCorrect(bool $isCorrect)
    {
        $this->isCorrect = $isCorrect;
    }

    /**
     * Returns the boolean state of isCorrect
     *
     * @return bool
     */
    public function isIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Returns the question
     *
     * @return \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $question
     */
    public function getPoolquestion()
    {
        return $this->poolquestion;
    }

    /**
     * Sets the question
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $question
     * @return void
     */
    public function setPoolquestion(\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $question)
    {
        $this->poolquestion = $question;
    }
}
