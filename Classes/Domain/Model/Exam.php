<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Domain\Model;

/**
 * This file is part of the "Exam Practice" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023
 */

/**
 * Exam
 */
class Exam extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * startTime
     *
     * @var int
     */
    protected $startTime = null;

    /**
     * questions
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $questions = null;

    /**
     * answers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $answers = null;

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->questions = $this->questions ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->answers = $this->answers ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the startTime
     *
     * @return int
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Sets the startTime
     *
     * @param int $startTime
     * @return void
     */
    public function setStartTime(int $startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * Adds a PoolQuestion
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $question
     * @return void
     */
    public function addQuestion(\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $question)
    {
        $this->questions->attach($question);
    }

    /**
     * Removes a PoolQuestion
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $questionToRemove The PoolQuestion to be removed
     * @return void
     */
    public function removeQuestion(\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion $questionToRemove)
    {
        $this->questions->detach($questionToRemove);
    }

    /**
     * Returns the questions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion>
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Sets the questions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolQuestion> $questions
     * @return void
     */
    public function setQuestions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $questions)
    {
        $this->questions = $questions;
    }

    /**
     * Adds a answer
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answer
     * @return void
     */
    public function addAnswer(\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answer)
    {
        $this->answers->attach($answer);
    }

    /**
     * Removes a answer
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $poolAnswerToRemove The PoolAnswer to be removed
     * @return void
     */
    public function removeAnswer(\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answerToRemove)
    {
        $this->answers->detach($answerToRemove);
    }

    /**
     * Returns the answers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer>
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Sets the answers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer> $answers
     * @return void
     */
    public function setAnswers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $answers)
    {
        $this->answers = $answers;
    }
}
