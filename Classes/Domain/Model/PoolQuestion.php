<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Domain\Model;


/**
 * This file is part of the "Exam Practice" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 
 */

/**
 * A Pool of questions
 */
class PoolQuestion extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * question
     *
     * @var string
     */
    protected $question = null;

    /**
     * correctAnswers
     *
     * @var int
     */
    protected $correctAnswers = null;

    /**
     * examType
     *
     * @var int
     */
    protected $examType = null;

    /**
     * category
     *
     * @var int
     */
    protected $category = null;

    /**
     * answers
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $answers = null;

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->answers = $this->answers ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Sets the question
     *
     * @param string $question
     * @return void
     */
    public function setQuestion(string $question)
    {
        $this->question = $question;
    }

    /**
     * Returns the correctAnswers
     *
     * @return int
     */
    public function getCorrectAnswers()
    {
        return $this->correctAnswers;
    }

    /**
     * Sets the correctAnswers
     *
     * @param int $correctAnswers
     * @return void
     */
    public function setCorrectAnswers(int $correctAnswers)
    {
        $this->correctAnswers = $correctAnswers;
    }

    /**
     * Returns the examType
     *
     * @return int
     */
    public function getExamType()
    {
        return $this->examType;
    }

    /**
     * Sets the examType
     *
     * @param int $examType
     * @return void
     */
    public function setExamType(int $examType)
    {
        $this->examType = $examType;
    }

    /**
     * Returns the category
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param int $category
     * @return void
     */
    public function setCategory(int $category)
    {
        $this->category = $category;
    }

    /**
     * Adds a PoolAnswer
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answer
     * @return void
     */
    public function addAnswer(\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answer)
    {
        $this->answers->attach($answer);
    }

    /**
     * Removes a PoolAnswer
     *
     * @param \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answerToRemove The PoolAnswer to be removed
     * @return void
     */
    public function removeAnswer(\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer $answerToRemove)
    {
        $this->answers->detach($answerToRemove);
    }

    /**
     * Returns the answers
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer>
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Sets the answers
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hagenbreak\ExamPractice\Domain\Model\PoolAnswer> $answers
     * @return void
     */
    public function setAnswers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $answers)
    {
        $this->answers = $answers;
    }
}
