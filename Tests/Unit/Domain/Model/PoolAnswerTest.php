<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class PoolAnswerTest extends UnitTestCase
{
    /**
     * @var \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getAnswerReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAnswer()
        );
    }

    /**
     * @test
     */
    public function setAnswerForStringSetsAnswer(): void
    {
        $this->subject->setAnswer('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('answer'));
    }

    /**
     * @test
     */
    public function getIsCorrectReturnsInitialValueForBool(): void
    {
        self::assertFalse($this->subject->getIsCorrect());
    }

    /**
     * @test
     */
    public function setIsCorrectForBoolSetsIsCorrect(): void
    {
        $this->subject->setIsCorrect(true);

        self::assertEquals(true, $this->subject->_get('isCorrect'));
    }
}
