<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class PoolQuestionTest extends UnitTestCase
{
    /**
     * @var \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getQuestionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getQuestion()
        );
    }

    /**
     * @test
     */
    public function setQuestionForStringSetsQuestion(): void
    {
        $this->subject->setQuestion('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('question'));
    }

    /**
     * @test
     */
    public function getCorrectAnswersReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getCorrectAnswers()
        );
    }

    /**
     * @test
     */
    public function setCorrectAnswersForIntSetsCorrectAnswers(): void
    {
        $this->subject->setCorrectAnswers(12);

        self::assertEquals(12, $this->subject->_get('correctAnswers'));
    }

    /**
     * @test
     */
    public function getExamTypeReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getExamType()
        );
    }

    /**
     * @test
     */
    public function setExamTypeForIntSetsExamType(): void
    {
        $this->subject->setExamType(12);

        self::assertEquals(12, $this->subject->_get('examType'));
    }

    /**
     * @test
     */
    public function getCategoryReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getCategory()
        );
    }

    /**
     * @test
     */
    public function setCategoryForIntSetsCategory(): void
    {
        $this->subject->setCategory(12);

        self::assertEquals(12, $this->subject->_get('category'));
    }

    /**
     * @test
     */
    public function getAnswersReturnsInitialValueForPoolAnswer(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getAnswers()
        );
    }

    /**
     * @test
     */
    public function setAnswersForObjectStorageContainingPoolAnswerSetsAnswers(): void
    {
        $answer = new \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer();
        $objectStorageHoldingExactlyOneAnswers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneAnswers->attach($answer);
        $this->subject->setAnswers($objectStorageHoldingExactlyOneAnswers);

        self::assertEquals($objectStorageHoldingExactlyOneAnswers, $this->subject->_get('answers'));
    }

    /**
     * @test
     */
    public function addAnswerToObjectStorageHoldingAnswers(): void
    {
        $answer = new \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer();
        $answersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $answersObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($answer));
        $this->subject->_set('answers', $answersObjectStorageMock);

        $this->subject->addAnswer($answer);
    }

    /**
     * @test
     */
    public function removeAnswerFromObjectStorageHoldingAnswers(): void
    {
        $answer = new \Hagenbreak\ExamPractice\Domain\Model\PoolAnswer();
        $answersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $answersObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($answer));
        $this->subject->_set('answers', $answersObjectStorageMock);

        $this->subject->removeAnswer($answer);
    }
}
