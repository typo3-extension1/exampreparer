<?php

declare(strict_types=1);

namespace Hagenbreak\ExamPractice\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class ExamTest extends UnitTestCase
{
    /**
     * @var \Hagenbreak\ExamPractice\Domain\Model\Exam|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Hagenbreak\ExamPractice\Domain\Model\Exam::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getStartTimeReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getStartTime()
        );
    }

    /**
     * @test
     */
    public function setStartTimeForIntSetsStartTime(): void
    {
        $this->subject->setStartTime(12);

        self::assertEquals(12, $this->subject->_get('startTime'));
    }

    /**
     * @test
     */
    public function getQuestionsReturnsInitialValueForPoolQuestion(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getQuestions()
        );
    }

    /**
     * @test
     */
    public function setQuestionsForObjectStorageContainingPoolQuestionSetsQuestions(): void
    {
        $question = new \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion();
        $objectStorageHoldingExactlyOneQuestions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneQuestions->attach($question);
        $this->subject->setQuestions($objectStorageHoldingExactlyOneQuestions);

        self::assertEquals($objectStorageHoldingExactlyOneQuestions, $this->subject->_get('questions'));
    }

    /**
     * @test
     */
    public function addQuestionToObjectStorageHoldingQuestions(): void
    {
        $question = new \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion();
        $questionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $questionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($question));
        $this->subject->_set('questions', $questionsObjectStorageMock);

        $this->subject->addQuestion($question);
    }

    /**
     * @test
     */
    public function removeQuestionFromObjectStorageHoldingQuestions(): void
    {
        $question = new \Hagenbreak\ExamPractice\Domain\Model\PoolQuestion();
        $questionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $questionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($question));
        $this->subject->_set('questions', $questionsObjectStorageMock);

        $this->subject->removeQuestion($question);
    }
}
