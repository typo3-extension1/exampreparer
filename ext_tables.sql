CREATE TABLE tx_exampractice_domain_model_poolquestion (
	question text NOT NULL DEFAULT '',
	correct_answers int(11) NOT NULL DEFAULT '0',
	exam_type int(11) DEFAULT '0' NOT NULL,
	category int(11) DEFAULT '0' NOT NULL,
	answers int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_exampractice_domain_model_poolanswer (
	poolquestion int(11) unsigned DEFAULT '0' NOT NULL,
	answer text NOT NULL DEFAULT '',
	is_correct smallint(1) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_exampractice_domain_model_exam (
	start_time int(11) NOT NULL DEFAULT '0',
	questions text NOT NULL,
	answers int(11) NOT NULL DEFAULT '0',
);

CREATE TABLE tx_exampractice_exams_questions_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

CREATE TABLE tx_exampractice_exams_answers_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);
