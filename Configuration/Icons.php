<?php

return [
    'exam_practice-plugin-takeexam' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:exam_practice/Resources/Public/Icons/user_plugin_takeexam.svg'
    ],
];
