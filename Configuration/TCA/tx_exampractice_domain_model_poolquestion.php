<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion',
        'label' => 'question',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'question',
        'iconfile' => 'EXT:exam_practice/Resources/Public/Icons/tx_exampractice_domain_model_poolquestion.gif'
    ],
    'types' => [
        '1' => ['showitem' => 'question, correct_answers, exam_type, category, answers, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language, sys_language_uid, l10n_parent, l10n_diffsource, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_exampractice_domain_model_poolquestion',
                'foreign_table_where' => 'AND {#tx_exampractice_domain_model_poolquestion}.{#pid}=###CURRENT_PID### AND {#tx_exampractice_domain_model_poolquestion}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'question' => [
            'exclude' => true,
            'label' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.question',
            'description' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.question.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'correct_answers' => [
            'exclude' => true,
            'label' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.correct_answers',
            'description' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.correct_answers.description',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'exam_type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.exam_type',
            'description' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.exam_type.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'category' => [
            'exclude' => true,
            'label' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.category',
            'description' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.category.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'answers' => [
            'exclude' => true,
            'label' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.answers',
            'description' => 'LLL:EXT:exam_practice/Resources/Private/Language/locallang_db.xlf:tx_exampractice_domain_model_poolquestion.answers.description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_exampractice_domain_model_poolanswer',
                'foreign_field' => 'poolquestion',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
    
    ],
];
